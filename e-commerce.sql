-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Des 2019 pada 02.10
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-commerce`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `booking`
--

CREATE TABLE `booking` (
  `booking_id` varchar(20) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `item_code` varchar(100) NOT NULL,
  `total_transaction` double NOT NULL,
  `total_price` double NOT NULL,
  `date_booking` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `booking`
--

INSERT INTO `booking` (`booking_id`, `buyer_id`, `item_code`, `total_transaction`, `total_price`, `date_booking`) VALUES
('BK0001', 2, 'B0009', 5, 250000, '2019-12-11 10:58:44'),
('BK0002', 2, 'B0009', 10, 500000, '2019-12-11 13:31:57'),
('BK0003', 2, 'B0009', 10, 500000, '2019-12-12 16:41:25'),
('BK0004', 2, 'B0009', 4, 200000, '2019-12-14 15:02:28'),
('BK0005', 4, 'B0009', 10, 500000, '2019-12-17 00:48:56');

--
-- Trigger `booking`
--
DELIMITER $$
CREATE TRIGGER `item_booking` AFTER INSERT ON `booking` FOR EACH ROW BEGIN
	UPDATE inventory SET stock=stock-NEW.total_transaction WHERE item_code = 		NEW.item_code;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `buyer`
--

CREATE TABLE `buyer` (
  `id_buyer` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` text NOT NULL,
  `no_telp` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buyer`
--

INSERT INTO `buyer` (`id_buyer`, `email`, `password`, `company_name`, `company_address`, `no_telp`) VALUES
(1, 'liza@gmail.com', 'dea95c352ad6fba4e1036dc51544cfc1', 'Liza Christina Garment', 'Jln.Selabintana', '08954231678'),
(2, 'timberwolf496@gmail.com', '69b358ad45b7f4610667505af41297cf', 'Timberwolf', 'Jln.cemerlang', '0895332231797'),
(4, 'malapasim@gmail.com', '840d7f2e95b7deb63d61fb1f9b6dee3a', 'Mala Corporation', 'Jln.Cicadas', '0895332231797');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(11) NOT NULL,
  `item_code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart`
--

INSERT INTO `cart` (`id_cart`, `item_code`) VALUES
(1, 'B0002'),
(2, 'B00011');

-- --------------------------------------------------------

--
-- Struktur dari tabel `installments`
--

CREATE TABLE `installments` (
  `install_id` varchar(20) NOT NULL,
  `booking_id` varchar(20) NOT NULL,
  `total_installments` double NOT NULL,
  `date_installments` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `installments`
--

INSERT INTO `installments` (`install_id`, `booking_id`, `total_installments`, `date_installments`) VALUES
('IS0001', 'BK0001', 50000, '2019-12-11 13:21:52'),
('IS0002', 'BK0002', 250000, '2019-12-11 13:31:57'),
('IS0003', 'BK0003', 50000, '2019-12-12 16:41:50'),
('IS0004', 'BK0004', 100000, '2019-12-14 15:02:29'),
('IS0005', 'BK0005', 0, '2019-12-17 00:48:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventory`
--

CREATE TABLE `inventory` (
  `item_code` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `size` varchar(10) NOT NULL,
  `stock` double NOT NULL,
  `price` double NOT NULL,
  `picture` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventory`
--

INSERT INTO `inventory` (`item_code`, `name`, `size`, `stock`, `price`, `picture`) VALUES
('B0001', 'BOOMBOGIE', 'M', -20, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_23.jpeg'),
('B00010', 'BOOMBOGIE', 'M', 40, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_31.jpeg'),
('B00011', 'QUICK SILVER', 'M', 30, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_32.jpeg'),
('B00012', 'QUICK SILVER', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_33.jpeg'),
('B00013', 'GREEN LIGHT', 'M', 100, 50000, 'gazebo.png'),
('B00014', 'Boombogie-Blank T-Shirt', 'M', 100, 50000, 'boombogie.png'),
('B0002', 'QUICK SILVER', 'M', 50, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_23_(1).jpeg'),
('B0003', 'GREEN LIGHT', 'M', 60, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_25.jpeg'),
('B0004', 'VANS', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_27.jpeg'),
('B0005', 'BILLABONG', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_28.jpeg'),
('B0006', 'QUICK SILVER', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_29_(1).jpeg'),
('B0007', 'BOOMBOGIE', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_29.jpeg'),
('B0008', 'BOOMBOGIE', 'M', 100, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_30.jpeg'),
('B0009', 'INSIGHT', 'M', 15, 50000, 'WhatsApp_Image_2019-11-05_at_08_32_31_(1).jpeg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `login`
--

CREATE TABLE `login` (
  `id_user` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `company_address` text NOT NULL,
  `no_telp` varchar(20) NOT NULL,
  `picture` varchar(50) NOT NULL,
  `is_active` enum('Active','Non Active') NOT NULL,
  `role_id` enum('Admin','Buyer') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `login`
--

INSERT INTO `login` (`id_user`, `email`, `password`, `company_name`, `company_address`, `no_telp`, `picture`, `is_active`, `role_id`) VALUES
(1, 'liza@gmail.com', 'dea95c352ad6fba4e1036dc51544cfc1', 'Liza Chrisitina Garment', 'Jln.selabintana', '08954231678', 'user.png', 'Active', 'Admin'),
(2, 'timberwolf496@gmail.com', '69b358ad45b7f4610667505af41297cf', 'Timberwolf', 'Jln.cemerlang', '0895332231797', '.png', 'Active', 'Buyer'),
(4, 'malapasim@gmail.com', '840d7f2e95b7deb63d61fb1f9b6dee3a', 'Mala Corporation', 'Jln.Cicadas', '0895332231797', '1576463289876.png', 'Active', 'Buyer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_details`
--

CREATE TABLE `order_details` (
  `id_buyer` int(11) NOT NULL,
  `item_code` varchar(100) NOT NULL,
  `install_id` varchar(20) NOT NULL,
  `total_buy` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `order_details`
--

INSERT INTO `order_details` (`id_buyer`, `item_code`, `install_id`, `total_buy`) VALUES
(2, 'B0009', 'IS0001', 5),
(2, 'B0009', 'IS0002', 10),
(2, 'B0009', 'IS0003', 10),
(2, 'B0009', 'IS0004', 4),
(4, 'B0009', 'IS0005', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `token`
--

CREATE TABLE `token` (
  `id_token` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indeks untuk tabel `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`id_buyer`);

--
-- Indeks untuk tabel `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indeks untuk tabel `installments`
--
ALTER TABLE `installments`
  ADD PRIMARY KEY (`install_id`);

--
-- Indeks untuk tabel `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`item_code`);

--
-- Indeks untuk tabel `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_token`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `buyer`
--
ALTER TABLE `buyer`
  MODIFY `id_buyer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `login`
--
ALTER TABLE `login`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `token`
--
ALTER TABLE `token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
