<?php
/**
* 
*/
class LoginModel extends CI_Model
{
	public function login($email,$password){
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$query	= $this->db->get('login');
		$cek	= $query->num_rows();

		if ($cek){
			$data = $query->row();
			$data_session = array(
				'id_user'			=>	$data->id_user,
				'email'				=>	$data->email,
				'company_name'		=>	$data->company_name,
				'company_address'	=>	$data->company_address,
				'no_telp'			=>	$data->no_telp,
				'picture'			=>	$data->picture,
				'is_active'			=>	$data->is_active,
				'role_id'			=>	$data->role_id,
				'status'			=> "login"
			);

			$this->session->set_userdata($data_session);
			$actives = $this->session->userdata('is_active');
			$roles =  $this->session->userdata('role_id');
			$company_name =  $this->session->userdata('company_name');
			if ($actives == 'Active') {
				if ($roles == 'Admin') {
					$this->session->set_flashdata(
						'message',
						"<script>
					window.onload=function(){
						swal('Success!','Your hasbeen log in!' + '$company_name','success')
					}
					</script>"
					);
					redirect('AdminController/dashboard');
				}else{
					$this->session->set_flashdata(
						'message',
						"<script>
					window.onload=function(){
						swal('Success!','Your hasbeen log in! ' + '$company_name','success')
					}
					</script>"
					);
					redirect('BuyerController');
				}
			}else{
				$this->session->set_flashdata(
					'message',
					"<script>
				window.onload=function(){
					swal('Warning!','Email Is Not Activated!','warning')
				}
				</script>"
				);
				redirect('LoginController');
			}
		}else{
			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Warning!','Email & Password Wrong!','warning')
			}
			</script>"
			);
			redirect('LoginController');
		}
	}
	public function add_user($table,$data){
		return $this->db->insert($table,$data);
	}
}