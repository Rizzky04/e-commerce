<?php
/**
 * 
 */
class BuyerModel extends CI_Model
{
	public function get_inven(){
		return $this->db->query('SELECT * FROM inventory');
	}
	public function update($code,$table,$data){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function get_cart(){
		return $this->db->query("SELECT cart.qty,cart.price,inventory.item_code,inventory.name,inventory.stock,inventory.price,inventory.picture FROM cart INNER JOIN inventory ON cart.item_code = inventory.item_code");
	}
	public function get_order(){
		$id 	=	$this->session->userdata('id_user');
		return $this->db->query("SELECT a.install_id,b.booking_id,c.company_name,.a.total_installments,b.total_price,a.date_installments FROM installments AS a INNER JOIN booking AS b ON a.booking_id = b.booking_id INNER JOIN buyer AS c ON b.buyer_id = c.id_buyer WHERE c.id_buyer='$id' ");
	}
	public function cart_delete($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
	}
	public function get_inventory(){
		return $this->db->query('SELECT count(*) as inven FROM inventory');
	}
	public function add_booking($table,$data){
		return $this->db->insert($table,$data);
	}
	public function add_payment($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	public function get_booking(){
		return $this->db->query('SELECT * FROM booking');
	}
	public function get_installments(){
		$this->db->select('RIGHT(installments.install_id,2) as install_id', FALSE);
		$this->db->order_by('install_id','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('installments');  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->install_id) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  }
				$batas = str_pad($kode, 2, "0", STR_PAD_LEFT);    
				$kodetampil = 'AG'.date('Ym').$batas;  //format kode
			  return $kodetampil;
	}

	public function kode(){
		$this->db->select('RIGHT(booking.booking_id,2) as booking_id', FALSE);
		$this->db->order_by('booking_id','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('booking');  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->booking_id) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  }
			  $batas = str_pad($kode, 2, "0", STR_PAD_LEFT);    
			  $kodetampil = 'TR'.date('Ym').$batas;  //format kode
			  return $kodetampil;
	}
}