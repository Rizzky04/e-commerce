<?php
/**
 * 
 */
class AdminModel extends CI_Model
{
	public function get_inven(){
		return $this->db->query('SELECT * FROM inventory');
	}
	public function insert_item($table,$data){
		$this->db->insert($table,$data);
	}
	public function get_user(){
		return $this->db->query('SELECT count(*) as jumlah FROM login');
	}
	public function get_inventory(){
		return $this->db->query('SELECT count(*) as inven FROM inventory');
	}
	public function get_order(){
		return $this->db->query("SELECT login.email,login.company_name,inventory.name,installments.install_id,installments.total_installments,order_details.total_buy FROM order_details INNER JOIN login ON order_details.id_buyer = login.id_user INNER JOIN inventory ON order_details.item_code = inventory.item_code INNER JOIN installments ON order_details.install_id = installments.install_id");
	}
	public function update_item($where,$table,$data){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function hapus($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	private function _deleteImage($kd_item)
	{
		$product = array('item_kode' => $kd_item);
		if ($product->image != "default.jpg") {
			$filename = explode(".", $product->image)[0];
			return array_map('unlink', glob(FCPATH . "assets/images/product/$filename.*"));
		}
	}
	public function get_gambar($kd_item){
		$this->db->where('item_code',$kd_item);
		return $this->db->get('inventory')->row();
	}
	public function kode(){
		$this->db->select('RIGHT(inventory.item_code,2) as item_code', FALSE);
		$this->db->order_by('item_code','DESC');    
		$this->db->limit(1);    
		$query = $this->db->get('inventory');  //cek dulu apakah ada sudah ada kode di tabel.    
		  if($query->num_rows() <> 0){      
			   //cek kode jika telah tersedia    
			   $data = $query->row();      
			   $kode = intval($data->item_code) + 1; 
		  }
		  else{      
			   $kode = 1;  //cek jika kode belum terdapat pada table
		  }
			  $batas = str_pad($kode, "0", STR_PAD_LEFT);    
			  $kodetampil = "B"."000".$batas;  //format kode
			  return $kodetampil;
	}

}