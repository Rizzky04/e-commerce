<?php $this->load->view('BuyerTemplate/header.php') ?>
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Change Password</h1>
                    <nav class="d-flex align-items-center">
                        <a href="<?= base_url('BuyerController/shop') ?>">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="<?= base_url('LoginController') ?>">Login/Register</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Login Box Area =================-->
    <section class="login_box_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <img class="img-fluid" src="<?= base_url('assets_buyer/img/login.jpg')?>" alt="">
                        <div class="hover">
                            <h4>New to our website?</h4>
                            <p>There are advances being made in science and technology everyday, and a good example of this is the</p>
                            <a data-target="#add" data-toggle="modal" class="primary-btn">Create an Account</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner">
                        <h3>Change your password for</h3>
                        <h5><?= $this->session->userdata('reset_email') ?></h5>
                        <form class="row login_form" action="<?= base_url('LoginController/ChangePassword') ?>" method="post" id="contactForm" novalidate="novalidate">
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password1" name="password1" placeholder="New Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'New Password'">
                                <?= form_error('password1','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password2" name="password2" placeholder="Repeat Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'New Password'">
                                <?= form_error('password2','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="primary-btn">Change Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->
    <?php $this->load->view('BuyerTemplate/footer.php') ?>