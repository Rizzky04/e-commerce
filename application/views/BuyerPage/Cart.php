<?php $this->load->view('BuyerTemplate/header.php') ?>

    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Shopping Cart</h1>
                    <nav class="d-flex align-items-center">
                        <a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="category.html">Cart</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Cart Area =================-->
    <section class="cart_area">
        <div class="container">
            <div class="cart_inner">
                <div class="table-responsive">
                    <table class="table">
                    <?= $this->session->flashdata('message') ?>
                        <thead>
                            <tr>
                                <th scope="col">Item Code</th>
                                <th scope="col">Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Total</th>
                                <th scope="col">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <form action="<?= base_url('BuyerController/booking_item') ?>" method="post">
                            <?php foreach ($inventory as $row) { ?>
            <input type="hidden" name="install_id" value="<?= $installments  ?>">
            <input type="hidden" name="booking_id" value="<?= $booking ?>">
            <input type="hidden" name="total_price" value="<?= $row->price ?>">
                            <?php } ?>
                            <?php foreach ($cart as $row) {
                                $code = $row->item_code;

                                ?>
                            <tr>
                                <td>
                                    <div class="media">
                                        <div class="d-flex">
                                            <img src="<?= base_url('assets/images/product/' . $row->picture) ?>" style="width: 100px; height: 50;" alt="">
                                        </div>
                                        <div class="media-body">
                                            <p><?= $row->name; ?></p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <h5>$<?= $row->price ?></h5>
                                </td>
                                <td>
                                    <div class="product_count">
                                        <input type="text" name="totbuy" id="totbuy" maxlength="12" value="1" title="Quantity:"
                                            class="input-text qty">
                                        <button onclick="var result = document.getElementById('totbuy'); var totbuy = result.value; if( !isNaN( totbuy )) result.value++;return false;"
                                            class="increase items-count" type="button"><i class="lnr lnr-chevron-up"></i></button>
                                        <button onclick="var result = document.getElementById('totbuy'); var totbuy = result.value; if( !isNaN( totbuy ) &amp;&amp; totbuy > 0 ) result.value--;return false;"
                                            class="reduced items-count" type="button"><i class="lnr lnr-chevron-down"></i></button>
                                    </div>
                                </td>
                                <td>
                                    <h5>$<?= $row->price*$row->stock ?></h5>
                                </td>
                                <input type="hidden" name="kd_item" value="<?= $code ?>">
                                <td>
                                    <h5><a class="primary-btn" href="<?= base_url('BuyerController/delete_cart/'.$code) ?>">delete</a></h5>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if($cart) { ?>
                                <td>
                                <div class="form-group">
                                    <label for="deposite">Deposite</label>
                                    <input type="number" class="form-control" name="deposite" id="deposite">
                                </div>
                                    <div class="checkout_btn_inner d-flex align-items-center">
                                        <a type="button" class="gray_btn" href="<?= base_url('BuyerController/shop') ?>">Continue Shopping</a>
                                        <button class="primary-btn" type="submit">Proceed to checkout</button>
                                    </div>
                                </td>
                                <?php } ?>
                            </form>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!--================End Cart Area =================-->

    <?php $this->load->view('BuyerTemplate/footer.php') ?>

<?php foreach ($inventory as $row) { ?>
<div id="add<?= $row->item_code ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?= base_url('BuyerController/booking_item') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <input type="hidden" name="install_id" value="<?= $installments  ?>">
            <input type="hidden" name="booking_id" value="<?= $booking ?>">
            <input type="hidden" name="total_price" value="<?= $row->price ?>">
            <input type="hidden" name="kd_item" value="<?= $row->item_code ?>">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-4" class="control-label">Deposite</label>
                        <input type="text" class="form-control" id="field-4" name="deposite" placeholder="Input DP">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>