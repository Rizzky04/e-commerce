<?php $this->load->view('BuyerTemplate/header.php') ?>
    <!-- start banner Area -->
    <section class="banner-area">
        <div class="container">
            <div class="row fullscreen align-items-center justify-content-start">
                <div class="col-lg-12">
                    <div class="active-banner owl-carousel">
                        <!-- single-slide -->
                        <div class="row single-slide align-items-center d-flex">
                            <div class="col-lg-5 col-md-6">
                                <div class="banner-content">
                                    <h1>T-Shirt New <br>Collection!</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                        dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
                                </div>
                            </div>
                            <div class="col-lg-7">
                                <div class="banner-img">
                                    <img class="img-fluid" src="<?= base_url('assets/images/product/boombogie.png')?>" alt="" style="width: 320px;">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End banner Area -->

    <?= $this->session->flashdata('message') ?>

    <!-- start features Area -->
    <section class="features-area section_gap">
        <div class="container">
            <div class="row features-inner">
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="<?= base_url('assets_buyer/img/features/f-icon1.png')?>" alt="">
                        </div>
                        <h6>Free Delivery</h6>
                        <p>Free Shipping on all order</p>
                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="<?= base_url('assets_buyer/img/features/f-icon2.png')?>" alt="">
                        </div>
                        <h6>Return Policy</h6>
                        <p>Free Shipping on all order</p>
                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="<?= base_url('assets_buyer/img/features/f-icon3.png')?>" alt="">
                        </div>
                        <h6>24/7 Support</h6>
                        <p>Free Shipping on all order</p>
                    </div>
                </div>
                <!-- single features -->
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-features">
                        <div class="f-icon">
                            <img src="<?= base_url('assets_buyer/img/features/f-icon4.png')?>" alt="">
                        </div>
                        <h6>Secure Payment</h6>
                        <p>Free Shipping on all order</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- end features Area -->

    <!-- Start category Area -->
    <section class="category-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 col-md-12">
                    <div class="row">
                        <div class="col-lg-8 col-md-8">
                            <div class="single-deal">
                                <div class="overlay"></div>
                                <img class="img-fluid w-100" src="<?= base_url('assets/images/product/aril.jpg')?>" alt="">
                                <a href="<?= base_url('assets_buyer/img/category/c1.jpg')?>" class="img-pop-up" target="_blank">
                                    <div class="deal-details">
                                        <h6 class="deal-title">Sneaker for Sports</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
            </div>
        </div>
    </section>
    <!-- End category Area -->

    <!-- start product Area -->
    <section class="owl-carousel active-product section_gap">
        <!-- single product slide -->
        <div class="single-product-slider">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <div class="section-title">
                            <h1>Latest Products</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                                dolore
                                magna aliqua.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($item as $row) { ?>
                    <!-- single product -->
                    <div class="col-lg-3 col-md-6">
                        <form method="post" action="<?= base_url('BuyerController/add_cart') ?>">
                        <div class="single-product">
                            <img class="img-fluid" src="<?= base_url('assets/images/product/' . $row->picture) ?>" alt="">
                            <div class="product-details">
                                <h6><?= $row->name ?></h6>
                                <div class="price">
                                    <h6>Stock : <?= $row->stock ?></h6><br>
                                    <h6>Price : <?= $row->price ?></h6>
                                </div>
                                
                                    <input type="hidden" name="code" value="<?= $row->item_code ?>">
                                    <input type="hidden" name="qty" value="1">
                                    <input type="hidden" name="price" value="<?= $row->price ?>">
                                    <input type="hidden" name="booking_id" value="<?= $booking ?>">
                                <div class="prd-bottom">
                                    <button class="btn btn-danger ti-bag" type="submit"></button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </section>

    <!-- Start brand Area -->
    <section class="brand-area section_gap">
        <div class="container">
            <div class="row">
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto" src="<?= base_url('assets_buyer/img/brand/1.png')?>" alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto" src="<?= base_url('assets_buyer/img/brand/2.png')?>" alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto" src="<?= base_url('assets_buyer/img/brand/3.png')?>" alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto" src="<?= base_url('assets_buyer/img/brand/4.png')?>" alt="">
                </a>
                <a class="col single-img" href="#">
                    <img class="img-fluid d-block mx-auto" src="<?= base_url('assets_buyer/img/brand/5.png')?>" alt="">
                </a>
            </div>
        </div>
    </section>
    <!-- End brand Area -->

    <?php $this->load->view('BuyerTemplate/footer.php') ?>