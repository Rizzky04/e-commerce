<?php $this->load->view('BuyerTemplate/header.php') ?>
	<!-- Start Banner Area -->
	<section class="banner-area organic-breadcrumb">
		<div class="container">
			<div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
				<div class="col-first">
					<h1>Confirmation</h1>
					<nav class="d-flex align-items-center">
						<a href="index.html">Home<span class="lnr lnr-arrow-right"></span></a>
						<a href="category.html">Confirmation</a>
					</nav>
				</div>
			</div>
		</div>
	</section>
	<!-- End Banner Area -->
	<!--================Order Details Area =================-->
	<section class="order_details section_gap">
		<div class="container">
			<div class="order_details_table">
				<h2>Order Details</h2>
				<?= $this->session->flashdata('message') ?>
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">Install ID</th>
								<th scope="col">Company Name</th>
								<th scope="col">Booking ID</th>
								<th scope="col">Total Installments</th>
								<th scope="col">Total Price</th>
								<th scope="col">Date Installments</th>
								<th scope="col">Advenced Payment</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($order as $row) {
								$install_id = $row->install_id;
								$id_buyer	= $row->company_name;
								$booking_id = $row->booking_id;
								$total_installments = $row->total_installments;
								$total_price	=	$row->total_price;
								$date_installments	= $row->date_installments;
							 ?>
							<tr>
								<td>
									<p><?= $install_id ?></p>
								</td>
								<td>
									<p><?= $id_buyer ?></p>
								</td>
								<td>
									<p><?= $booking_id ?></p>
								</td>
								<td>
									<h5><?= $total_installments ?></h5>
								</td>
								<td>
									<h5><?= $total_price ?></h5>
								</td>
								<td>
									<p><?= $date_installments ?></p>
								</td>
								<td>
								<?php if($total_installments <= $total_price) { ?>
									<h5><button data-toggle="modal" data-target="#edit<?= $row->install_id ?>" class="btn btn-default">Advenced Payment</button></h5>
									<?php } ?>
								</td>
								
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	<!--================End Order Details Area =================-->
    <?php $this->load->view('BuyerTemplate/footer.php') ?>

    <!-- Modal -->
<?php foreach ($order as $row) { ?>
<div id="edit<?= $row->install_id ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?= base_url('BuyerController/advenced_payment') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <input type="hidden" name="install_id" value="<?= $row->install_id ?>">
            <input type="hidden" name="bayaran" value="<?= $row->total_installments ?>">
            <input type="hidden" name="total"	value="<?= $row->total_price ?>">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Add Payment</label>
                        <input type="text" class="form-control" id="field-1" name="payment">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>