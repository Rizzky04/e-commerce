            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboard</a>
                                        </li>
                                        <li class="active">
                                            Dashboard
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

            <div class="row">
                <div class="col-lg-3 col-md-4">
                    <div class="text-center card-box">
                        <div class="member-card">
                            
                            <div class="thumb-xl member-thumb m-b-10 center-block">
                            <img src="http://localhost/new-recognition/assets_admin/images/users/users.png" class="img-circle img-thumbnail" alt="Admin HR" style="max-height: 128px">
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <button type="button" class="btn btn-custom btn-bordered waves-light waves-effect w-md m-b-5" data-toggle="modal" onclick="con_modal_edit()">Edit</button>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>User Id</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('id_user') ?></div>
                            </div>
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>Company Name</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('company_name') ?></div>
                            </div>
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>Company Address</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('company_address') ?></div>
                            </div>
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>Phone Numeber</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('no_telp') ?></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>Email</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('email') ?></div>
                            </div>
                            
                            <div class="row m-b-15">
                                <div class="col-md-4"><label>Role ID</label></div>
                                <div class="col-md-8"><?= $this->session->userdata('role_id') ?></div>
                            </div>
                        </div>
                    </div>   
            
        </div> <!-- container -->

    </div> <!-- content -->
                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->