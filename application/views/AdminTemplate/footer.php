            </div>
            <!-- END wrapper -->



            <script>
                var resizefunc = [];
            </script>

            <!-- jQuery  -->
            <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
            <script src="<?= base_url('assets/js/detect.js') ?>"></script>
            <script src="<?= base_url('assets/js/fastclick.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.blockUI.js') ?>"></script>
            <script src="<?= base_url('assets/js/waves.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.slimscroll.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.scrollTo.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/switchery/switchery.min.js') ?>"></script>

            <!-- Sweet-Alert  -->
            <script src="<?= base_url('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') ?>"></script>
            <script src="<?= base_url('assets/pages/jquery.sweet-alert.init.js') ?>"></script>

            <!-- Counter js  -->
            <script src="<?= base_url('assets/plugins/waypoints/jquery.waypoints.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/counterup/jquery.counterup.min.js') ?>"></script>

            <!-- Flot chart js -->
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.time.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.tooltip.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.resize.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.pie.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.selection.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/flot-chart/jquery.flot.crosshair.js') ?>"></script>

            <script src="<?= base_url('assets/plugins/moment/moment.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>


            <!-- Dashboard init -->
            <script src="<?= base_url('assets/pages/jquery.dashboard_2.js') ?>"></script>

            <script src="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.js') ?>"></script>

            <script src="<?= base_url('assets/plugins/datatables/dataTables.buttons.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/buttons.bootstrap.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/jszip.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/pdfmake.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/vfs_fonts.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/buttons.html5.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/buttons.print.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.fixedHeader.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.keyTable.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.responsive.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/responsive.bootstrap.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.scroller.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.colVis.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/datatables/dataTables.fixedColumns.min.js') ?>"></script>

            <!-- init -->
            <script src="<?= base_url('assets/pages/jquery.datatables.init.js') ?>"></script>

            <!-- App js -->
            <script src="<?= base_url('assets/js/jquery.core.js') ?>"></script>
            <script src="<?= base_url('assets/js/jquery.app.js') ?>"></script>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('#datatable').dataTable();
                    $('#datatable-keytable').DataTable({
                        keys: true
                    });
                    $('#datatable-responsive').DataTable();
                    $('#datatable-colvid').DataTable({
                        "dom": 'C<"clear">lfrtip',
                        "colVis": {
                            "buttonText": "Change columns"
                        }
                    });
                    $('#datatable-scroller').DataTable({
                        ajax: "<?= base_url('assets/plugins/datatables/json/scroller-demo.json') ?>",
                        deferRender: true,
                        scrollY: 380,
                        scrollCollapse: true,
                        scroller: true
                    });
                    var table = $('#datatable-fixed-header').DataTable({
                        fixedHeader: true
                    });
                    var table = $('#datatable-fixed-col').DataTable({
                        scrollY: "300px",
                        scrollX: true,
                        scrollCollapse: true,
                        paging: false,
                        fixedColumns: {
                            leftColumns: 1,
                            rightColumns: 1
                        }
                    });
                });
                TableManageButtons.init();
            </script>

            <!-- Modal-Effect -->
            <script src="<?= base_url('assets/plugins/custombox/js/custombox.min.js') ?>"></script>
            <script src="<?= base_url('assets/plugins/custombox/js/legacy.min.js') ?>"></script>

            <script>
                $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
                $('#reportrange').daterangepicker({
                    format: 'MM/DD/YYYY',
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment(),
                    minDate: '01/01/2012',
                    maxDate: '12/31/2016',
                    dateLimit: {
                        days: 60
                    },
                    showDropdowns: true,
                    showWeekNumbers: true,
                    timePicker: false,
                    timePickerIncrement: 1,
                    timePicker12Hour: true,
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    opens: 'left',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-success',
                    cancelClass: 'btn-default',
                    separator: ' to ',
                    locale: {
                        applyLabel: 'Submit',
                        cancelLabel: 'Cancel',
                        fromLabel: 'From',
                        toLabel: 'To',
                        customRangeLabel: 'Custom',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                });
            </script>

            <script>
            function del(id){
                swal({
                    title: "Are you sure?",
                    text: "You will not able to recover this imaginary file! "+ id,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                },function(){
                    location.href="<?= base_url().'AdminController/delete/' ?>"+id;
                });
            }
            </script>

            </body>

            </html>