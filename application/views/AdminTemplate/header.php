<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?= base_url('assets/images/favicon.ico')?>">
        <!-- App title -->
        <title>Zircos - Responsive Admin Dashboard Template</title>

        <!-- Sweet Alert -->
        <link href="<?= base_url('assets/plugins/bootstrap-sweetalert/sweet-alert.css') ?>" rel="stylesheet" type="text/css">

        <!-- date range picker -->
        <link href="<?= base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">

        <!-- Custom box css -->
        <link href="<?= base_url('assets/plugins/custombox/css/custombox.min.css') ?>" rel="stylesheet">

        <!-- DataTables -->
        <link href="<?= base_url('assets/plugins/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/buttons.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/responsive.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/scroller.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/dataTables.colVis.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/dataTables.bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/plugins/datatables/fixedColumns.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/core.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/components.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/icons.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/pages.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/menu.css')?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/responsive.css')?>" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="<?= base_url('assets/plugins/switchery/switchery.min.css')?>">

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= base_url('assets/js/modernizr.min.js')?>"></script>

    </head>


    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">