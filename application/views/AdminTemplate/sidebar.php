            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="menu-title">Navigation</li>

                            <li>
                                <a href="<?= base_url('AdminController/dashboard') ?>" class="waves-effect"><i class="fa fa-dashboard"></i><span> Dashboard </span></a>
                            </li>
                            <li>
                                <a href="<?= base_url('AdminController/inventory') ?>" class="waves-effect"><i class=" mdi mdi-cart"></i><span> Inventory </span></a>
                            </li>
                            <li>
                                <a href="<?= base_url('AdminController/buyer') ?>" class="waves-effect"><i class=" mdi mdi-account-multiple"></i><span> Buyer Data </span></a>
                            </li>
                            
                        </ul>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                    <div class="help-box">
                        <h5 class="text-muted m-t-0">For Help ?</h5>
                        <p class=""><span class="text-custom">Email:</span> <br/> support@support.com</p>
                        <p class="m-b-0"><span class="text-custom">Call:</span> <br/> (+123) 123 456 789</p>
                    </div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->
