            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboard</a>
                                        </li>
                                        <li class="active">
                                            Dashboard
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>Inventory T-shirt</b></h4>
                                    <?= $this->session->flashdata('error') ?>
                                    <?= $this->session->flashdata('message') ?>
                                    <div class="text-right" style="margin-bottom: 15px;">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-custom waves-effect w-md waves-light m-b-5" data-target="#add" data-toggle="modal">Add Item <i class="mdi mdi-plus"></i></button>
                                        </div>
                                    </div>
                                    <table id="datatable-responsive"
                                           class="table table-striped  table-colored table-info dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Item Code</th>
                                            <th>Name</th>
                                            <th>Size</th>
                                            <th>Stock</th>
                                            <th>Price</th>
                                            <th>Picture</th>
                                            <th style="text-align: center;">#</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $id=1; foreach ($inventory as $row) { ?>
                                        <tr>
                                            <td><?= $id++ ?></td>
                                            <td><?= $row->item_code ?></td>
                                            <td><?= $row->name ?></td>
                                            <td><?= $row->size ?></td>
                                            <td><?= $row->stock ?></td>
                                            <td><?= $row->price ?></td>
                                            <td><img src="<?= base_url('assets/images/product/' . $row->picture) ?>" style="width: 50px; height: 50px;"></td>
                                            <td style="text-align: center;">
                                            <button data-toggle="modal" data-target="#edit<?= $row->item_code ?>" class="btn btn-custom"><i class="fa fa-pencil"></i></button>
                                            <button class="btn btn-danger waves-effect waves-light btn-sm" onclick="del('<?= $row->item_code ?>')"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
<!-- Modal -->
<div id="add" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Item</h4>
      </div>
      <form action="<?= base_url('AdminController/add_item') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Kode Item</label>
                        <input type="text" class="form-control" id="field-1" name="kd_item" value="<?= $kode ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-2" class="control-label">Name</label>
                        <input type="text" class="form-control" id="field-2" name="name" placeholder="Name Item">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Size</label>
                        <select class="form-control" id="field-3" name="size">
                            <option>Size Item</option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-4" class="control-label">Stock</label>
                        <input type="number" class="form-control" id="field-4" name="stock" placeholder="Stock Item">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-5" class="control-label">Price</label>
                        <input type="number" class="form-control" id="field-5" name="price" placeholder="Price Item">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-6" class="control-label">Picture</label>
                        <input type="file" class="form-control" id="field-6" name="picture">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<?php foreach ($inventory as $row) { ?>
<div id="edit<?= $row->item_code ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Item</h4>
      </div>
      <form action="<?= base_url('AdminController/edit_item') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <input type="hidden" name="id" value="<?= $row->picture ?>">
            <input type="hidden" name="kd" value="<?= $row->item_code ?>">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Kode Item</label>
                        <input type="text" class="form-control" id="field-1" name="kd_item" value="<?= $row->item_code ?>" readonly="readonly">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-2" class="control-label">Name</label>
                        <input type="text" class="form-control" id="field-2" name="name" value="<?= $row->name ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Size</label>
                        <select class="form-control" id="field-3" name="size">
                            <option value="<?= $row->size ?>"><?= $row->size ?></option>
                            <option value="M">M</option>
                            <option value="L">L</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-4" class="control-label">Stock</label>
                        <input type="number" class="form-control" id="field-4" name="stock" value="<?= $row->stock ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-5" class="control-label">Price</label>
                        <input type="number" class="form-control" id="field-5" name="price" value="<?= $row->price ?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <img src="<?= base_url('assets/images/product/' . $row->picture) ?>" style="width: 45px; height: 50;">
                        <input type="file" class="form-control" id="field-6" name="picture">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>