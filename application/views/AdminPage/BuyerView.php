            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Dashboard</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Zircos</a>
                                        </li>
                                        <li>
                                            <a href="#">Dashboard</a>
                                        </li>
                                        <li class="active">
                                            Dashboard
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
                    
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">

                                    <h4 class="m-t-0 header-title"><b>Buyer Data</b></h4>
                                    <?= $this->session->flashdata('error') ?>
                                    <div class="text-right" style="margin-bottom: 15px;">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-custom waves-effect w-md waves-light m-b-5" data-target="#add" data-toggle="modal">Add Item <i class="mdi mdi-plus"></i></button>
                                            <a href="#" class="btn btn-danger waves-effect w-md waves-light m-b-5">Print to PDF</a>
                                            <a href="#" class="btn btn-custom waves-effect w-md waves-light m-b-5">Print to Excel</a>
                                        </div>
                                    </div>
                                    <form action="<?= base_url('AdminController/pay_done') ?>" method="post">
                                    <table id="datatable-responsive"
                                           class="table table-striped  table-colored table-info dt-responsive nowrap" cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name Buyer</th>
                                            <th>Name Code</th>
                                            <th>Install ID</th>
                                            <th>Total Installments</th>
                                            <th>Total Buy</th>
                                            <th>Option</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $id=1; foreach ($detail as $row) { ?>
                                        <tr>
                                            <input type="hidden" name="email" value="<?= $row->email ?>">
                                            <td><?= $id++ ?></td>
                                            <td><?= $row->company_name ?></td>
                                            <td><?= $row->name ?></td>
                                            <td><?= $row->install_id ?></td>
                                            <td><?= $row->total_installments ?></td>
                                            <td><?= $row->total_buy ?></td>
                                            <td>
                                            <button type="submit" class="btn btn-primary waves-effect w-md waves-light m-b-3">Process</button>
                                            <button type="submit" class="btn btn-custom waves-effect w-md waves-light m-b-3">Done</button></td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2016 © Zircos.
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->