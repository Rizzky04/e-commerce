0<?php $this->load->view('BuyerTemplate/header.php') ?>
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Register</h1>
                    <nav class="d-flex align-items-center">
                        <a href="<?= base_url('BuyerController/shop') ?>">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="<?= base_url('LoginController') ?>">Login/Register</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Login Box Area =================-->
    <section class="login_box_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <img class="img-fluid" src="<?= base_url('assets_buyer/img/login.jpg')?>" alt="">
                        <div class="hover">
                            <h4>New to our website?</h4>
                            <p>There are advances being made in science and technology everyday, and a good example of this is the</p>
                            <a href="<?= base_url('LoginController') ?>" class="primary-btn">Back to login</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner">
                        <h3>Register your account</h3>
                        <form class="row login_form" action="<?= base_url('LoginController/add') ?>" method="post" enctype="multipart/form-data" id="contactForm" novalidate="novalidate">
                        <input type="hidden" name="is_active" value="2">
                        <input type="hidden" name="role_id" value="2">
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?= set_value('email') ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'">
                                <?= form_error('email','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password1" name="password1" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                                <?= form_error('password1','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password2" name="password2" placeholder="Repeat Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Repeat Password'">
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="<?= set_value('company_name') ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Company Name'">
                                <?= form_error('company_name','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="text" class="form-control" id="company_address" name="company_address" placeholder="Company Address" value="<?= set_value('company_address') ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Company Address'">
                                <?= form_error('company_address','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="number" class="form-control" id="no_telp" name="no_telp" placeholder="Phone Number" value="<?= set_value('no_telp') ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Phone Number'">
                                <?= form_error('no_telp','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="file" class="form-control" id="picture" name="picture" placeholder="Select Picture" value="<?= set_value('picture') ?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Select Picture'">
                                <?= form_error('picture','<small class="text-danger pl-3">','</small>') ?>
                            </div>
                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="primary-btn">Register</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->
    <?php $this->load->view('BuyerTemplate/footer.php') ?>