<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="<?= base_url('assets_buyer/img/fav.png')?>">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Liza Shop</title>

    <link href="<?= base_url('assets_buyer/sweetalert/sweet-alert.css') ?>" rel="stylesheet" type="text/css">
    <!--
        CSS
        ============================================= -->
    <link rel="stylesheet" href="<?= base_url('assets_buyer/bootstrap/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/linearicons.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/themify-icons.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/bootstrap.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/owl.carousel.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/nice-select.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/nouislider.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/ion.rangeSlider.css')?>" />
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/ion.rangeSlider.skinFlat.css')?>" />
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/magnific-popup.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets_buyer/css/main.css')?>">
</head>

<body>

    <!-- Start Header Area -->
    <header class="header_area sticky-header">
        <div class="main_menu">
            <nav class="navbar navbar-expand-lg navbar-light main_box">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <a class="navbar-brand logo_h" href="<?= base_url('BuyerController/shop') ?>"><img src="<?= base_url('assets_buyer/img/liza.png')?>" style="width: 150px;" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                     aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
                        <ul class="nav navbar-nav menu_nav ml-auto">
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('BuyerController') ?>">Home</a></li>
                            <?php
                            if ($this->session->userdata('status') == 'login') { ?>
                             <li class="nav-item"><a class="nav-link" href="<?= base_url('BuyerController/order_data') ?>">Data Order</a></li>
                            <?php
                            }else{

                            }
                            ?>

                            <?php
                            if ($this->session->userdata('status') == 'login') { ?>
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('LoginController/my_profile') ?>">My Profile</a></li>
                            <?php
                            }else{
                                
                            }
                            ?>

                            <?php
                            if ($this->session->userdata('status') == 'login') { ?>
                             <li class="nav-item"><a class="nav-link" href="<?= base_url('LoginController/logout') ?>">Log Out</a></li>
                            <?php
                            }else{ ?>
                            <li class="nav-item"><a class="nav-link" href="<?= base_url('LoginController/logout') ?>">Sign In</a></li>
                            <?php
                            }
                            ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-item"><a href="<?= base_url('BuyerController/cart') ?>" class="cart"><span class="ti-bag"></span></a></li>
                            <li class="nav-item">
                                <button class="search"><span class="lnr lnr-magnifier" id="search"></span></button>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="search_input" id="search_input_box">
            <div class="container">
                <form class="d-flex justify-content-between">
                    <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                    <button type="submit" class="btn"></button>
                    <span class="lnr lnr-cross" id="close_search" title="Close Search"></span>
                </form>
            </div>
        </div>
    </header>
    <!-- End Header Area -->