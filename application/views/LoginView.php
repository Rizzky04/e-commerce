<?php $this->load->view('BuyerTemplate/header.php') ?>
    <!-- Start Banner Area -->
    <section class="banner-area organic-breadcrumb">
        <div class="container">
            <div class="breadcrumb-banner d-flex flex-wrap align-items-center justify-content-end">
                <div class="col-first">
                    <h1>Login/Register</h1>
                    <nav class="d-flex align-items-center">
                        <a href="<?= base_url('BuyerController/shop') ?>">Home<span class="lnr lnr-arrow-right"></span></a>
                        <a href="<?= base_url('LoginController') ?>">Login/Register</a>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->

    <!--================Login Box Area =================-->
    <section class="login_box_area section_gap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="login_box_img">
                        <img class="img-fluid" src="<?= base_url('assets_buyer/img/login.jpg')?>" alt="">
                        <div class="hover">
                            <h4>New to our website?</h4>
                            <p>There are advances being made in science and technology everyday, and a good example of this is the</p>
                            <a href="<?= base_url('LoginController/register') ?>" class="primary-btn">Create an Account</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="login_form_inner">
                        <h3>Log in to enter</h3>
                        <?= $this->session->flashdata('message') ?>
                        <form class="row login_form" action="<?= base_url('LoginController/proses') ?>" method="post" id="contactForm" novalidate="novalidate">
                            <div class="col-md-12 form-group">
                                <input type="Email" class="form-control" id="email" name="email" placeholder="Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'">
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'">
                            </div>
                            <div class="col-md-12 form-group">
                                <div class="creat_account">
                                    <input type="checkbox" id="f-option2" name="selector">
                                    <label for="f-option2">Keep me logged in</label>
                                </div>
                            </div>
                            <div class="col-md-12 form-group">
                                <button type="submit" value="submit" class="primary-btn">Log In</button>
                                <a href="<?= base_url('LoginController/password') ?>">Forgot Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Login Box Area =================-->
    <?php $this->load->view('BuyerTemplate/footer.php') ?>
    <!-- Modal -->
<div id="add" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form action="<?= base_url('LoginController/add') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <div class="row">
            <input type="hidden" name="picture" value="user.jpg">
            <input type="hidden" name="is_active" value="2">
            <input type="hidden" name="role_id" value="2">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Email</label>
                        <input type="text" class="form-control" id="field-1" name="email" placeholder="Email">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-2" class="control-label">Password</label>
                        <input type="password" class="form-control" id="field-2" name="password" placeholder="Password">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-4" class="control-label">Company Name</label>
                        <input type="text" class="form-control" id="field-4" name="company_name" placeholder="Company Name">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-5" class="control-label">Company Address</label>
                        <input type="text" class="form-control" id="field-5" name="company_address" placeholder="Company Address">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-6" class="control-label">Phone Number</label>
                        <input type="number" class="form-control" id="field-6" name="no_telp" placeholder="Phone Number">
                    </div>
                </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Add</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
      </form>
    </div>
  </div>
</div>