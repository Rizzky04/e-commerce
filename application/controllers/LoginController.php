<?php
/**
* 
*/
class LoginController extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('LoginModel');
	}
	public function index(){
		$this->load->view('LoginView');
	}
	public function register(){
		$this->load->view('RegisterView');
	}
	public function proses(){
		$email = $this->input->post('email');
		$password = md5($this->input->post('password'));

		$this->LoginModel->login($email,$password);
	}
	public function logout(){
		$this->session->sess_destroy();

		$this->session->set_flashdata(
			'message',
			"<script>
		window.onload=function(){
			swal('Success!','You have been logout!','success')
		}
		</script>"
		);
		redirect('LoginController');
	}
	public function add(){
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[buyer.email]',[
			'is_unique' => 'This email has already registered!'
		]);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[8]|matches[password2]',[
			'matches' 		=> 'Password dont match!',
			'min_length' 	=> 'Password to short'
		]);
		$this->form_validation->set_rules('password2', 'Repeat Password', 'required|trim|matches[password1]');
		$this->form_validation->set_rules('company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('company_address', 'Company Address', 'required');
		$this->form_validation->set_rules('no_telp', 'Phone Number', 'required');
		$email = $this->input->post('email');

		if ($this->form_validation->run() == false) {
			$this->load->view('RegisterView');
		}else{
		$data = array(
			'email'				=>	$email,
			'password'			=>	md5($this->input->post('password1')),
			'company_name'		=>	$this->input->post('company_name'),
			'company_address'	=>	$this->input->post('company_address'),
			'no_telp'			=>	$this->input->post('no_telp')
		);
		$data2 = array(
			'email'				=>	$email,
			'password'			=>	md5($this->input->post('password1')),
			'company_name'		=>	$this->input->post('company_name'),
			'company_address'	=>	$this->input->post('company_address'),
			'no_telp'			=>	$this->input->post('no_telp'),
			'picture'			=>	$this->_do_upload(),
			'is_active'			=>	$this->input->post('is_active'),
			'role_id'			=>	$this->input->post('role_id')
		);
		$this->LoginModel->add_user('buyer',$data);
		$this->LoginModel->add_user('login',$data2);

		$token = base64_encode(random_bytes(32));
		$user_token = array(
			'email' => $email,
			'token'	=> $token,
			'date_created'	=> time()
		);

		$this->db->insert( 'token',$user_token);

		$this->_sendEmail($token,'verify');
		
		$this->session->set_flashdata(
			'message',
			"<script>
		window.onload=function(){
			swal('Success!','Congratulation! your account has been created. Please Activated!','success')
		}
		</script>"
		);
		redirect('LoginController');
		}
	}
	public function _do_upload()
	{
		$config['upload_path']          = 'assets_buyer/img/user/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name']            = round(microtime(true) * 1000);
		//$config['max_size']             = 1000000;
		//$config['max_width']            = 1000000;
		//$config['max_height']           = 1000000;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('picture')) {
			return "default.jpg";
		} else {
			return $this->upload->data('file_name');
		}
	}
	private function _sendEmail($token,$type){
		$config = array(
			'protocol'	=>	'smtp',
			'smtp_host'	=>	'ssl://smtp.googlemail.com',
			'smtp_user'	=>	'rizzkyardyansyah@gmail.com',
			'smtp_pass'	=>	'rizzky12345',
			'smtp_port'	=>	465,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
            'newline' 	=> "\r\n"
		);

		$this->email->initialize($config);

		$this->email->from('rizzkyardyansyah@gmail.com', 'Rizky Ardiansyah');
		$this->email->to($this->input->post('email'));

		if($type == 'verify'){
		$this->email->subject('Account Veryfication');
		$this->email->message('Click this link to verify you account : <a href="'. base_url() . 'LoginController/verify?email=' . $this->input->post('email') . '&token=' .urlencode($token) . '">Activate</a>');
		}elseif ($type == 'forgot') {
		$this->email->subject('Reset Password');
		$this->email->message('Click this link to reset your account : <a href="'. base_url() . 'LoginController/reset_password?email=' . $this->input->post('email') . '&token=' .urlencode($token) . '">Reset Password</a>');
		}

		if($this->email->send()){
			return true;
		}else{
			echo $this->email->print_debugger();
			die;
		}
	}
	public function verify(){
		$email 	=	$this->input->get('email');
		$token 	=	$this->input->get('token');

		$user 	=	$this->db->get_where('login', ['email' => $email])->row_array();

		if ($user) {
			$token = $this->db->get_where('token', ['token' => $token])->row_array();

			if ($token) {
				if (time() - $token['date_created'] < (60 * 60 * 24)){
					$this->db->set('is_active', 1);
					$this->db->where('email', $email);
					$this->db->update('login');

					$this->db->delete('token', ['email' => $email]);

					$this->session->set_flashdata(
						'message',
						"<script>
					window.onload=function(){
						swal('Success!',''.$email.' has been activated! please login','success')
					}
					</script>"
					);
					redirect('LoginController');
				}else{
					$this->db->delete('login', ['email' => $email]);
					$this->db->delete('buyer', ['email' => $email]);
					$this->db->delete('token', ['email' => $email]);
					$this->session->set_flashdata(
						'message',
						"<script>
					window.onload=function(){
						swal('Warning!','Account activation failed! Token expired','warning')
					}
					</script>"
					);
				}
			}else{
				$this->session->set_flashdata(
					'message',
					"<script>
				window.onload=function(){
					swal('Warning!','Account activation failed! Wrong token','Warning')
				}
				</script>"
				);
			}
		}else{
			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Warning!','Account activation failed! Wrong email','warning')
			}
			</script>"
			);
		}
	}
	public function password(){
		$this->load->view('ForgotPasswordView');
	}
	public function forgot_password(){
		$email 	=	$this->input->post('email');
		$user 	=	$this->db->get_where('login', ['email' => $email, 'is_active' => 1])->row_array();

		if ($user) {
			$token 		= base64_encode(random_bytes(32));
			$user_token = [
						'email' => $email,
						'token'	=> $token,
						'date_created'	=> time()
						];

			$this->db->insert('token',$user_token);
			$this->_sendEmail($token, 'forgot');

			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Success!','Please check email to reset your password!','success')
			}
			</script>"
			);
			redirect('LoginController');

		}else{
			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Warning!','Email is not registered or activated!','warning')
			}
			</script>"
			);
		}
	}
	public function reset_password(){
		$email = $this->input->get('email');
		$token = $this->input->get('token');

		$user 	=	$this->db->get_where('login', ['email' => $email])->row_array();

		if ($user) {
			$user_token = $this->db->get_where('token', ['token' => $token])->row_array();

			if ($user_token) {
				$this->session->set_userdata('reset_email', $email);
				$this->ChangePassword();
			}else{
				$this->session->set_flashdata(
					'message',
					"<script>
				window.onload=function(){
					swal('Warning!','Reset password failed! Wrong token','warning')
				}
				</script>"
				);
			}
		}else{
			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Warning!','Reset password failed! Wrong email','warning')
			}
			</script>"
			);
		}
	}
	public function ChangePassword(){
		if (!$this->session->userdata('reset_email')) {
			redirect('LoginController');
		}
		$this->form_validation->set_rules('password1', 'Password', 'trim|required|min_length[8]|matches[password2]');
		$this->form_validation->set_rules('password2', 'Repeat Password', 'trim|required|min_length[8]|matches[password1]');
		if ($this->form_validation->run() == false) {
			$this->load->view('ChangePasswordView');
		}else{

			$password = md5($this->input->post('password1'));
			$email = $this->session->userdata('reset_email');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('login');

			$this->db->set('password', $password);
			$this->db->where('email', $email);
			$this->db->update('buyer');

			$this->session->unset_userdata('reset_email');

			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Success!','Your password has been repair! Please Login','success')
			}
			</script>"
			);
			redirect('LoginController');
		}
	}
}