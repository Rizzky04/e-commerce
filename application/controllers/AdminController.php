<?php
/**
 * 
 */
class AdminController extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('AdminModel');
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Denied! you should login first!</div>');
			redirect(base_url('LoginController'));
		}
	}
	public function my_profile(){
		$this->load->view('AdminTemplate/header');
		$this->load->view('AdminTemplate/topbar');
		$this->load->view('AdminTemplate/sidebar');
		$this->load->view('AdminPage/ProfileView');
		$this->load->view('AdminTemplate/footer');		
	}
	public function dashboard(){
		$data['user']	=	$this->AdminModel->get_user()->result();
		$data['item']	=	$this->AdminModel->get_inventory()->result();

		$this->load->view('AdminTemplate/header');
		$this->load->view('AdminTemplate/topbar');
		$this->load->view('AdminTemplate/sidebar');
		$this->load->view('AdminPage/DashboardView',$data);
		$this->load->view('AdminTemplate/footer');
	}
	public function inventory(){
		$data['inventory']	=	$this->AdminModel->get_inven()->result();
		$data['kode']		= 	$this->AdminModel->kode();
		$this->load->view('AdminTemplate/header');
		$this->load->view('AdminTemplate/topbar');
		$this->load->view('AdminTemplate/sidebar');
		$this->load->view('AdminPage/InventoryView',$data);
		$this->load->view('AdminTemplate/footer');
	}
	public function buyer(){
		$data['detail']		=	$this->AdminModel->get_order()->result();
		$this->load->view('AdminTemplate/header');
		$this->load->view('AdminTemplate/topbar');
		$this->load->view('AdminTemplate/sidebar');
		$this->load->view('AdminPage/BuyerView',$data);
		$this->load->view('AdminTemplate/footer');		
	}
	public function add_item(){
		$kode 	= $this->input->post('kd_item');
		$name 	= $this->input->post('name');
		$size 	= $this->input->post('size');
		$stock	= $this->input->post('stock');
		$price	= $this->input->post('price');

			$data = array(
			'item_code' => $kode,
			'name'		=> $name,
			'size'		=> $size,
			'stock'		=> $stock,
			'price'		=> $price
			);

		if (empty($_FILES['picture']['name'])) {
			$picture = $this->_do_upload();
			$data['picture'] = "default.jpg";
		}elseif(!empty($_FILES['picture']['name'])){
			$picture = $this->_do_upload();
			$data['picture'] = $picture;
		}
		
		$this->AdminModel->insert_item('inventory',$data);
		$this->session->set_flashdata('message', 
		"<script>
		window.onload=function(){
			swal('Added','Data Berhasil Ditambah','success')
		}
		</script>");
		redirect('AdminController/inventory');
	}
	public function edit_item(){
		$id 	= $this->input->post('id');
		$kd 	= $this->input->post('kd');
		$kode 	= $this->input->post('kd_item');
		$name 	= $this->input->post('name');
		$size 	= $this->input->post('size');
		$stock	= $this->input->post('stock');
		$price	= $this->input->post('price');

			$data = array(
			'item_code' => $kode,
			'name'		=> $name,
			'size'		=> $size,
			'stock'		=> $stock,
			'price'		=> $price
			);
		$where=array('item_code'=>$kd);
		unlink(base_url('assets/images/product/'.$picture));
		$this->AdminModel->update_item($where,'inventory',$data);
		$this->session->set_flashdata('message', 
		"<script>
		window.onload=function(){
			swal('Added','Data Berhasil Ditambah','success')
		}
		</script>");
		redirect('AdminController/inventory');
	}
	public function delete($kd_item){
		$where=array('item_code'=>$kd_item);
		$this->AdminModel->hapus($where,'inventory');
		$this->session->set_flashdata(
			'message',
			"<script>
		window.onload=function(){
			swal('Delete','Data Berhasil Di Hapus','success')
		}
		</script>"
		);
		redirect('AdminController/inventory');
	}
	public function _do_upload()
	{
		$config['upload_path']          = 'assets/images/product/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png';
		$config['file_name']            = round(microtime(true) * 1000);
		//$config['max_size']             = 1000000;
		//$config['max_width']            = 1000000;
		//$config['max_height']           = 1000000;

		$this->load->library('upload', $config);

		//if(!$this->upload->do_upload('picture')){
		//	$error = array('error' => $this->Upload->display_errors() );
		// }

		if (!$this->upload->do_upload('picture')) {
			return "default.jpg";
		} else {
			return $this->upload->data('file_name');
		}
	
	}
	public function pay_done(){
		$this->_sendEmail();
		redirect('AdminController/buyer');

	}
	private function _sendEmail(){
		$email = $this->input->post('email');
		$config = array(
			'protocol'	=>	'smtp',
			'smtp_host'	=>	'ssl://smtp.googlemail.com',
			'smtp_user'	=>	'rizzkyardyansyah@gmail.com',
			'smtp_pass'	=>	'rizzky12345',
			'smtp_port'	=>	465,
			'mailtype'	=>	'html',
			'charset'	=>	'utf-8',
            'newline' 	=> "\r\n"
		);

		$this->email->initialize($config);

		$this->email->from('rizzkyardyansyah@gmail.com', 'Rizky Ardiansyah');
		$this->email->to($email);

		$this->email->subject('Item Done');
		$this->email->message('Your item hasbeen done to proses');

		if($this->email->send()){
			return true;
		}else{
			echo $this->email->print_debugger();
			die;
		}
	}
}