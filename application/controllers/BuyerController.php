<?php
/**
 * 
 */
class BuyerController extends CI_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->model('BuyerModel');
	}
	public function index(){
		$data['item']			=	$this->BuyerModel->get_inven()->result();
		$data['inventory']		=	$this->BuyerModel->get_inven()->result();
		$data['booking']		=	$this->BuyerModel->kode();
		$data['installments']	=	$this->BuyerModel->get_installments();
		$this->load->view('BuyerPage/ShopView',$data);
	}
	public function qty(){
		$id  = $this->input->post('id');
		$qty = $this->input->post('totbuy');
		$data = array(
			'qty' => $qty
		);
		$where = array('id_cart' => $id);

		$this->BuyerModel->update($where,'cart',$data);
	}
	public function cart(){
		$data['cart']			=	$this->BuyerModel->get_cart()->result();
		$data['item']			=	$this->BuyerModel->get_inven()->result();
		$data['inventory']		=	$this->BuyerModel->get_inven()->result();
		$data['booking']		=	$this->BuyerModel->kode();
		$data['installments']	=	$this->BuyerModel->get_installments();
		$this->load->view('BuyerPage/Cart', $data);
	}
	public function add_cart(){
		$booking = $this->input->post('booking_id');
		$code = $this->input->post('code');
		$qty  = $this->input->post('qty');
		$price= $this->input->post('price');
		$data = array(
			'booking_id' => $booking,
			'item_code' => $code,
			'qty'	=> $qty,
			'price'	=> $price
		);
		$this->db->insert('cart', $data);
		redirect('BuyerController/cart');
	}
	public function delete_cart($code){
		$where = array('item_code'=>$code);
		$this->BuyerModel->cart_delete($where,'cart');
		$this->session->set_flashdata(
			'message',
			"<script>
		window.onload=function(){
			swal('Delete','Data Berhasil Di Hapus','success')
		}
		</script>"
		);
		redirect('BuyerController/cart');
	}
	public function order_data(){
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Denied! you should login first!</div>');
			redirect(base_url('LoginController'));
		}

		$data['order']	=	$this->BuyerModel->get_order()->result();
		$this->load->view('BuyerPage/OrderDataView', $data);
	}
	public function advenced_payment(){
		$install_id = $this->input->post('install_id'); 
		$bayaran	= $this->input->post('bayaran');
		$total 		= $this->input->post('total');
		$akhir 		= $this->input->post('payment');

		$bayar 		= $bayaran+$akhir;
		$selesai	= $bayar-$total;

		$data = array(
			'install_id'		=> $install_id,
			'total_installments'=> 'Lunas'
		);

		$where = array('install_id' => $install_id);

		if ($bayar >= $total) {
			$this->BuyerModel->add_payment($where,$data,'installments');
			$this->session->set_flashdata(
				'message',
				"<script>
			window.onload=function(){
				swal('Success','Your payment is success!' + $selesai,'success')
			}
			</script>"
			);
			redirect('BuyerController/order_data');
		}else{
			echo "<script>alert('Duit Kurang')</script>";
		}
		// $this->session->set_flashdata(
		// 	'message',
		// 	"<script>
		// window.onload=function(){
		// 	swal('Success','Your payment is success!','success')
		// }
		// </script>"
		// );
		// redirect('BuyerController/order_data');
	}
	public function booking_item(){
		if ($this->session->userdata('status') != "login") {
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">If you wont to buy item! you should login first!</div>');
			redirect (base_url('LoginController'));
		}
		$code 		 = $this->input->post('kd_item');
		$total_price = $this->input->post('total_price');
		$totbuy 	 = $this->input->post('totbuy');
		$booking = $this->input->post('booking_id');

		$total 		 = $totbuy*$total_price;
		$data = array(
			'booking_id'			=>	$booking,
			'buyer_id'				=>	$this->session->userdata('id_user'),
			'item_code'				=>	$this->input->post('kd_item'),
			'total_transaction'		=>	$totbuy,
			'total_price'			=>	$total
		);

		$data2 = array(
			'install_id'			=>	$this->input->post('install_id'),
			'booking_id'			=>	$booking,
			'total_installments'	=>	$this->input->post('deposite')
		);
		$data3 = array(
			'id_buyer'				=>	$this->session->userdata('id_user'),
			'item_code'				=>	$this->input->post('kd_item'),
			'install_id'			=>	$this->input->post('install_id'),
			'total_buy'				=>	$this->input->post('totbuy')
		);
		$data4 = array('booking_id' => $booking);

		$this->BuyerModel->cart_delete($data4,'cart');
		$this->BuyerModel->add_booking('booking',$data);
		$this->BuyerModel->add_booking('installments',$data2);
		$this->BuyerModel->add_booking('order_details',$data3);
		$this->session->set_flashdata(
			'message',
			"<script>
		window.onload=function(){
			swal('Success','Your item hasbeen buy!','success')
		}
		</script>"
		);
		redirect('BuyerController');
	}
}